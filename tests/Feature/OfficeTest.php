<?php

namespace Tests\Feature;

use App\Models\Office;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class OfficeTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        Office::factory(50)->create();

        $this->getJson(route('offices.index'))
            ->assertStatus(200)
            ->assertJson(Office::orderBy('name')->get()->toArray());
    }

    public function testOfficeCreatedSuccessfully()
    {
        $office = [
            'name' => 'John Doe',
            'address' => 'Central Park'
        ];

        $this->postJson(route('offices.store'), $office, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJson($office);
    }

    public function testOfficeReadSuccessfully()
    {
        $office = Office::factory()->createOne();

        $this->getJson(route('offices.show', $office->id))
            ->assertStatus(200)
            ->assertJson($office->toArray());
    }

    public function testOfficeReadNotFound()
    {
        $this->getJson(route('offices.show', 1))
            ->assertStatus(404);
    }

    public function testOfficeReadCacheResponse()
    {
        $office = Office::factory()->createOne();

        Cache::shouldReceive('get')
            ->once()
            ->with('office_' . $office->id)
            ->andReturn(null);

        Cache::shouldReceive('put')
            ->once()
            ->andSet('office_' . $office->id, $office->toArray());

        $this->getJson(route('offices.show', $office->id))
            ->assertStatus(200)
            ->assertJson($office->toArray());
    }

    public function testOfficeUpdatedSuccessfully()
    {
        $office = Office::factory()->createOne();

        $officeUpdated = [
            'name' => 'John Doe',
            'address' => 'Central Park'
        ];

        $this->putJson(route('offices.update', $office->id), $officeUpdated, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson($officeUpdated);

        $officeUpdated = [
            'name' => 'Jane Doe',
            'address' => '5th Avenue'
        ];

        $this->patchJson(route('offices.update', $office->id), $officeUpdated, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson($officeUpdated);
    }

    public function testOfficeUpdatedNotFound()
    {
        $this->putJson(route('offices.show', 1))
            ->assertStatus(404);

        $this->patchJson(route('offices.show', 1))
            ->assertStatus(404);
    }

    public function testOfficeDeletedSuccessfully()
    {
        $office = Office::factory()->createOne();

        $this->deleteJson(route('offices.destroy', $office->id))
            ->assertStatus(204);
    }

    public function testOfficeDeletedNotFound()
    {
        $this->deleteJson(route('offices.show', 1))
            ->assertStatus(404);
    }
}
