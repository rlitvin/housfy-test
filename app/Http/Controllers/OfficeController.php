<?php

namespace App\Http\Controllers;

use App\Jobs\CacheResponse;
use App\Models\Office;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class OfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(Office::orderBy('name')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $office = new Office();
        $office->name = $request->input('name');
        $office->address = $request->input('address');
        $office->save();

        return response()->json($office, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $office = Cache::get('office_' . $id);

        if ($office == null) {
            $office = Office::find($id);
            if ($office == null) return response()->json([], Response::HTTP_NOT_FOUND);
            CacheResponse::dispatchAfterResponse($office);
        }

        return response()->json($office);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $office = Office::find($id);
        if ($office == null) return response()->json([], Response::HTTP_NOT_FOUND);

        $office->name = $request->input('name');
        $office->address = $request->input('address');
        $office->save();
        Cache::forget('office_' . $office->id);
        return response()->json($office);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $office = Office::find($id);
        if ($office == null) return response()->json([], Response::HTTP_NOT_FOUND);

        Cache::forget('office_' . $office->id);
        $office->delete();
        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
