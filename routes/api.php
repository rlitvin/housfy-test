<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OfficeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('offices')->name('offices.')->group(function() {
    Route::get('/', [OfficeController::class, 'index'])->name('index');
    Route::post('/', [OfficeController::class, 'store'])->name('store');
    Route::get('{office}', [OfficeController::class, 'show'])->name('show');
    Route::match(['put', 'patch'], '{office}', [OfficeController::class, 'update'])->name('update');
    Route::delete('{office}', [OfficeController::class, 'destroy'])->name('destroy');
});

Route::fallback(function () {
    return response()->json([], \Illuminate\Http\Response::HTTP_NOT_FOUND);
});
